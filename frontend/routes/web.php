<?php

$routeGroup=[
    'prefix' => Localization::setLocale(),
    'middleware' => [
        //'localeSessionRedirect',
        'localizationRedirect',
        'localeViewPath',
        'redirectTrailingSlash',
        'caching'
    ]
];

Route::group($routeGroup, function () {
    if(env('APP_ENV')!=='production') {
        Route::get('/html/{slug?}', 'HtmlController@page');
    }

    Route::get('/', 'PostController@index')->name('homepage');

    Route::post('/posts/get', 'PostController@getPosts')->name('get_posts');
    Route::get('/posts/{slug}', 'PostController@show')->name('show_post');

    Route::post('/categories/get', 'CategoryController@getAllCategory')->name('get_categories');

    Route::post('/tags/get', 'TagController@getAllTags')->name('get_tags');
});