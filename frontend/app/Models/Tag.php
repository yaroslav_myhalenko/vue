<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

class Tag extends \LaravelOctoberModel
{
    use \TranslatableTrait;
    use \LongreadTrait;
    use \ModelTrait;

    public $table = 'vue_vue_tags';

    public $backendModel = 'Vue\Vue\Models\Tags';

    public $similar;

    public $timestamps = false;

    public $translatable = [
        ['name', 'index' => true],
    ];

    protected $longread = [
        'longread'
    ];

    public function scopeSort(Builder $query)
    {
        return $query->orderBy('sort_order', 'ASC');
    }
}