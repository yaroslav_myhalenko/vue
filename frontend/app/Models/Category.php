<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

class Category extends \LaravelOctoberModel
{
    use \TranslatableTrait;
    use \LongreadTrait;
    use \ModelTrait;

    public $table = 'vue_vue_categories';

    public $backendModel = 'Vue\Vue\Models\Categories';

    public $similar;

    public $timestamps = false;

    public $translatable = [
        ['name', 'index' => true],
    ];

    protected $longread = [
        'longread'
    ];

    public function scopeSort(Builder $query)
    {
        return $query->orderBy('sort_order', 'ASC');
    }

    public function scopeIsEnabled(Builder $query)
    {
        return $query->where('is_show', true);
    }
}