<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Post extends \LaravelOctoberModel
{
    use \TranslatableTrait;
    use \LongreadTrait;
    use \ModelTrait;

    public $table = 'vue_vue_posts';

    public $backendModel = 'Vue\Vue\Models\Posts';

    public $timestamps = false;

    protected $perPage = 20;

    public $similar;

    public $translatable = [
        ['name', 'index' => true],
    ];

    protected $longread = [
        'longread'
    ];

    public $attachments = [
        'imagePreview',
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'vue_vue_post_tags', 'posts_id', 'tags_id');
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'vue_vue_post_categories', 'posts_id', 'categories_id');
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(Authors::class);
    }

    public function scopeIsShow(Builder $query)
    {
        return $query->where('vue_vue_posts.is_show', true);
    }

    public function scopeSort(Builder $query, $sort)
    {
        if (!$sort) {
            return $query->orderBy('id', 'ASC');
        } else {
            return $query->orderBy('id', $sort);
        }
    }

    public function scopeByCategory(Builder $query, $category)
    {
        if ($category) {
            return $query->select('vue_vue_posts.*')
                ->join('vue_vue_post_categories', 'vue_vue_post_categories.posts_id', 'vue_vue_posts.id')
                ->join('vue_vue_categories', 'vue_vue_post_categories.categories_id', 'vue_vue_categories.id')
                ->where('vue_vue_categories.slug', '=', $category)
            ;
        }

        return $query;
    }

    public function scopeByAuthor(Builder $query, $author)
    {
        if ($author) {
            $author = Authors::where('slug', '=', $author)->first();
            return $query->where('author_id', '=', $author->id);
        }

        return $query;
    }

    public function scopeByTag(Builder $query, $tag)
    {
        if ($tag) {
            return $query->select('vue_vue_posts.*')
                ->join('vue_vue_post_tags', 'vue_vue_post_tags.posts_id', 'vue_vue_posts.id')
                ->join('vue_vue_tags', 'vue_vue_post_tags.tags_id', 'vue_vue_tags.id')
                ->where('vue_vue_tags.slug', '=', $tag)
            ;
        }

        return $query;
    }

    public function scopeWhereInTags(Builder $query, $arrTags, $postId)
    {
        if ($arrTags) {
            return $query->select('vue_vue_posts.*')
                ->join('vue_vue_post_tags', 'vue_vue_post_tags.posts_id', 'vue_vue_posts.id')
                ->join('vue_vue_tags', 'vue_vue_tags.id', 'vue_vue_post_tags.tags_id')
                ->whereIn('vue_vue_tags.id', $arrTags)
                ->where('vue_vue_posts.id', '!=', $postId);
        }

        return $query;
    }

    public function scopeWhereInCategories(Builder $query, $arrCategories, $postId)
    {
        if ($arrCategories) {
            return $query->select('vue_vue_posts.*')
                ->join('vue_vue_post_categories', 'vue_vue_post_categories.posts_id', 'vue_vue_posts.id')
                ->join('vue_vue_categories', 'vue_vue_categories.id', 'vue_vue_post_categories.categories_id')
                ->whereIn('vue_vue_categories.id', $arrCategories)
                ->where('vue_vue_posts.id', '!=', $postId);
        }

        return $query;
    }

}