<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

class Authors extends \LaravelOctoberModel
{
    use \TranslatableTrait;
    use \LongreadTrait;
    use \ModelTrait;

    public $table = 'vue_vue_author';

    public $backendModel = 'Vue\Vue\Models\Authors';

    public $similar;

    public $timestamps = false;

    public $translatable = [
        ['name', 'index' => true],
    ];

    protected $longread = [
        'longread'
    ];

    public $attachments = [
        'avatar',
    ];

    public function scopeSort(Builder $query)
    {
        return $query->orderBy('sort_order', 'ASC');
    }
}