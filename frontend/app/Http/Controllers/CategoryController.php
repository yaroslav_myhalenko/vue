<?php


namespace App\Http\Controllers;


use App\Models\Category;

class CategoryController extends BaseController
{
    public function getAllCategory()
    {
        $categories = Category::isEnabled()->get();

        return response()->json(['categories' => $categories]);
    }
}