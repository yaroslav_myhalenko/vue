<?php


namespace App\Http\Controllers;


use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends BaseController
{
    public function index()
    {
        return $this->view('pages/homepage');
    }

    public function getPosts(Request $request)
    {
        $posts = Post::byCategory($request->get('category'))
            ->isShow()
            ->byTag($request->get('tag'))
            ->byAuthor($request->get('author'))
            ->with(['category', 'tags', 'author', 'imagePreview'])
            ->sort($request->get('sort'))
            ->paginate()
        ;

        $posts->map(function ($post) {
            $post->author->setAttribute('avatar', $post->author->avatar->path);
            $post->imagePreview->setAttribute('url', $post->imagePreview->path);
            return $post;
        });

        return response()->json(['posts' => $posts]);
    }

    public function show($slug)
    {
        $post = Post::where('slug', '=', $slug)
            ->with(['category', 'tags', 'author', 'imagePreview'])
            ->first();

        if (!$post) {
            return redirect()->route('homepage');
        }

        $post->author->avatar = $post->author->avatar->path;
        $post->imagePreview->url = $post->imagePreview->path;

        $arrTags = [];
        $arrCategories = [];

        foreach ($post->tags as $tag) {
            $arrTags[] = $tag->id;
        }

        foreach ($post->category as $category) {
            $arrCategories[] = $category->id;
        }

        $relevantPosts = Post::whereInCategories($arrCategories, $post->id)
            ->whereInTags($arrTags, $post->id)
            ->with(['category', 'tags', 'author', 'imagePreview'])
            ->distinct()
            ->take(4)
            ->get();

        $relevantPosts->map(function ($relevant) {
            $relevant->author->setAttribute('avatar', $relevant->author->avatar->path);
            $relevant->imagePreview->setAttribute('url', $relevant->imagePreview->path);
            return $relevant;
        });

        return $this->view('pages/post', ['post' => $post, 'relevants' => $relevantPosts]);
    }
}