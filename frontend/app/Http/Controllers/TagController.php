<?php


namespace App\Http\Controllers;


use App\Models\Tag;

class TagController extends BaseController
{
    public function getAllTags()
    {
        $tags = Tag::all();

        return response()->json(['tags' => $tags]);
    }
}