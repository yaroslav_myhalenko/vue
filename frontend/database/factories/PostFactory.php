<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'category_id' => $faker->numberBetween(1, 10),
        'title' => $faker->sentence(),
        'description' => $faker->text(100),
        'author' => $faker->name(),
        'img' => "https://loremflickr.com/640/480?random=".$faker->numberBetween(1, 10),
        'date' => $faker->date()
    ];
});
