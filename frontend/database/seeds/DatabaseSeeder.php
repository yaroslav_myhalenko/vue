<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Category::class, 10)->create();
        $tags = factory(App\Models\Tag::class, 10)->create()->pluck('id')->toArray();;

        factory(App\Models\Post::class, 200)->create()->each(function ($post) use ($tags){
            $post->tags()->saveMany(App\Models\Tag::all()->random(1, count($tags)));
        });
    }
}
