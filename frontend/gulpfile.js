var prod = false;

var gulp = require('gulp'),
	gutil = require('gulp-util'),
	pug = require('gulp-pug'),
	sass = require('gulp-sass'),
	babel = require('gulp-babel'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	htmlmin = require('gulp-htmlmin'),
	elixir = require('laravel-elixir'),
	autoprefixer = require('gulp-autoprefixer');

var path = {
	dist: {
		js: 'public/storage/js/',
		css: 'public/storage/css/',
		img: 'public/storage/img/',
		imgElixir: 'public/build/storage/img/',
		fonts: 'public/build/storage/css/fonts',
	},
	src: {
		js: 'resources/assets/js/*.js',
		style: 'resources/assets/sass/*.*',
		img: 'resources/assets/img/**/*.*',
		fonts: 'resources/assets/fonts/icomoon/fonts/*.*'
	}
};
//
// SASS
gulp.task('sass', function () {
	var outputStyle = prod ? 'compressed' : 'expanded';
	if (prod) {
		sourcemaps = {};
		sourcemaps.init = gutil.noop;
		sourcemaps.write = gutil.noop;
	}
	gulp.src(path.src.style)
		.pipe(sourcemaps.init())
		.pipe(sass({
			soursemap: !prod,
			outputStyle: outputStyle
		}).on('error', sass.logError))
		.pipe(autoprefixer({browsers:['last 15 versions', '> 5%', 'safari 8', 'not ie <= 8', 'opera 12.1', 'ios 7']}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.dist.css));
});
// //
// // SCRIPTS
// gulp.task('scripts', function(){
// 	if (!prod) {
// 		uglify = gutil.noop;
// 	}
// 	gulp.src(path.src.js)
// 		.pipe(babel({
// 			presets: ['es2015']
// 		}))
// 		.pipe(uglify())
// 		.pipe(gulp.dest(path.dist.js));
// });

// IMAGES
gulp.task('images', function(){
	gulp.src(path.src.img)
		.pipe(gulp.dest(path.dist.img))
		.pipe(gulp.dest(path.dist.imgElixir));
});

// FONTS
gulp.task('fonts', function(){
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.dist.fonts))
		.pipe(gulp.dest(path.dist.fontsElixir));
});

gulp.task('compress', function() {
	var opts = {
		collapseWhitespace: true,
		removeAttributeQuotes: true,
		removeComments: true,
		minifyJS: true
	};
	return gulp.src('./storage/framework/views/*')
		.pipe(htmlmin(opts))
		.pipe(gulp.dest('./storage/framework/views/'));
});

gulp.task('default', ['images', 'fonts', 'compress']);

elixir(function(mix) {
	mix.scripts([
		'./resources/assets/js/js/jquery-3.3.1.min.js',
		'./resources/assets/js/js/*.js',
	], 'public/storage/js');

	mix.styles([
		"./resources/assets/css/*.css",
		"./resources/assets/fonts/flaticon/font/flaticon.css",
		"./resources/assets/fonts/icomoon/style.css",
		// "./public/storage/css/build.css",
	], 'public/storage/css');

	mix.version(['public/storage/css/all.css', 'public/storage/js/all.js']);
});


