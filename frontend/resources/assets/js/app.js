import Vue from 'vue';
import VueRouter from 'vue-router';
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
window.axios = require('axios');

Vue.use(VueRouter);
// Vue.use(BootstrapVue);
// Vue.use(IconsPlugin);

Vue.component('home', require('./components/Home').default);
Vue.component('post-body', require('./components/PostBody').default);
Vue.component('post-header', require('./components/PostHeader').default);
Vue.component('nav-post', require('./components/NavPost').default);

// const routes = [
// ];
//
const router = new VueRouter({
    mode: 'history',
});

const app = new Vue({
    el: '#app',
    router,
    data: {
        isHover: false,
        isScrollDown: false,
        documentLastScrollTop: 0
    },
    methods: {
        handleIsHover: function() {
            this.isHover = !('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);
        },
        handleDocumentScroll: function() {
            var st = window.pageYOffset || document.documentElement.scrollTop;
            
            this.isScrollDown = st>this.documentLastScrollTop;

            this.documentLastScrollTop = st <= 0 ? 0 : st;
        },
    },
    computed: {
    },
    mounted () {
        this.handleIsHover();
    },
    created () {
    },
    destroyed () {
    }
});

Vue.config.devtools = false;
Vue.config.debug = true;
Vue.config.silent = false;