@spaceless
    <!doctype html>
    <html lang="{{ Localization::getCurrentLocale() }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
            <meta name="csrf-token" content="%%csrf_token%%">
            <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Playfair+Display:400,700,900" rel="stylesheet">

            <link rel="stylesheet" href="{{elixir('storage/css/all.css')}}" type="text/css">
        </head>
        <body>
            
            <div id="app" class="site-wrap" v-bind:class="{ 'is-hover': isHover, 'is-scroll-down': isScrollDown }">
                <div class="site-mobile-menu">
                    <div class="site-mobile-menu-header">
                        <div class="site-mobile-menu-close mt-3">
                            <span class="icon-close2 js-menu-toggle"></span>
                        </div>
                    </div>
                    <div class="site-mobile-menu-body"></div>
                </div>

                @yield('content')

            </div>

            <script src="{{ asset('js/app.js') }}"></script>
            <script src="{{ elixir('storage/js/all.js') }}"></script>
        </body>
    </html>
@endspaceless
