@extends('layouts.app')

@section('content')
    <div>
        <nav-post></nav-post>
        <post-header
                :post="{{ $post }}"
        ></post-header>
        <post-body
                :post="{{ $post }}"
                :relevants="{{ $relevants }}"
        ></post-body>
    </div>
@endsection
