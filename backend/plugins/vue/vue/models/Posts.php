<?php namespace Vue\Vue\Models;

use Model;

/**
 * Model
 */
class Posts extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title' => 'required',
        'author' => 'required',
        'category' => 'required',
        'tags' => 'required',
        'date' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'vue_vue_posts';

    public $belongsTo = [
//        'category' => 'Vue\Vue\Models\Categories',
        'author' => 'Vue\Vue\Models\Authors',
    ];

    public $attachOne = [
        'imagePreview' => 'System\Models\File'
    ];

    public function getCategoryOptions()
    {
        $categories =  Categories::where('is_enabled', true)->pluck('name', 'id');

        return $categories;
    }

    public $belongsToMany = [
        'tags' => ['Vue\Vue\Models\Tags', 'table' => 'vue_vue_post_tags', 'posts_id', 'tags_id'],
        'category' => ['Vue\Vue\Models\Categories', 'table' => 'vue_vue_post_categories', 'posts_id', 'categories_id'],
    ];

    public function getTagsOptions()
    {
        $tags = Tags::where('is_enabled', true)->pluck('name', 'id');

        return $tags;
    }

    public function getAuthorOptions()
    {
        $authors = Authors::where('is_enabled', true)->pluck('name', 'id');

        return $authors;
    }
}
