<?php namespace Vue\Vue\Models;

use Model;

/**
 * Model
 */
class Authors extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'avatar' => 'required',
        'about' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'vue_vue_author';

    public $attachOne = [
        'avatar' => 'System\Models\File'
    ];
}
