<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVueVuePosts extends Migration
{
    public function up()
    {
        Schema::create('vue_vue_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('category_id');
            $table->string('title');
            $table->string('description');
            $table->string('author');
            $table->string('img');
            $table->dateTime('date');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vue_vue_posts');
    }
}
