<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVueCategories3 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_categories', function($table)
        {
            $table->boolean('is_menu');
            $table->boolean('is_enabled');
        });
    }
    
    public function down()
    {
        Schema::table('vue_vue_categories', function($table)
        {
            $table->dropColumn('is_menu');
            $table->dropColumn('is_enabled');
        });
    }
}
