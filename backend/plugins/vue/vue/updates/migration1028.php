<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1028 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_post_categories', function($table)
        {
            $table->foreign('posts_id', 'post_categories_foreign')->references('id')->on('vue_vue_posts')->onDelete('cascade');
            $table->foreign('category_id', 'categories_post_foreign')->references('id')->on('vue_vue_categories')->onDelete('cascade');
            $table->renameColumn('category_id', 'categories_id');
        });
    }

    public function down()
    {
        Schema::table('vue_vue_post_categories', function($table)
        {
            $table->renameColumn('categories_id', 'category_id');
            $table->dropForeign('post_categories_foreign');
            $table->dropForeign('categories_post_foreign');
        });
    }
}