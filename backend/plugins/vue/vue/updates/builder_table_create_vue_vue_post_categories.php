<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVueVuePostCategories extends Migration
{
    public function up()
    {
        Schema::create('vue_vue_post_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('posts_id');
            $table->integer('category_id');
            $table->primary(['posts_id','category_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vue_vue_post_categories');
    }
}
