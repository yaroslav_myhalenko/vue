<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVuePostTags extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_post_tags', function($table)
        {
            $table->dropPrimary(['post_id','tag_id']);
            $table->renameColumn('post_id', 'posts_id');
            $table->primary(['posts_id','tag_id']);
        });
    }
    
    public function down()
    {
        Schema::table('vue_vue_post_tags', function($table)
        {
            $table->dropPrimary(['posts_id','tag_id']);
            $table->renameColumn('posts_id', 'post_id');
            $table->primary(['post_id','tag_id']);
        });
    }
}
