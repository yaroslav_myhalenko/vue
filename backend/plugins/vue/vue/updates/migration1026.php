<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1026 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_posts', function($table)
        {
            $table->index('slug');
            $table->index('author_id');
            $table->index('is_top');
            $table->index('is_show');
        });
    }

    public function down()
    {
        Schema::table('vue_vue_posts', function($table)
        {
            $table->dropIndex('slug');
            $table->dropIndex('author_id');
            $table->dropIndex('is_top');
            $table->dropIndex('is_show');
        });
    }
}