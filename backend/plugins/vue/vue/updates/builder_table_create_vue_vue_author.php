<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVueVueAuthor extends Migration
{
    public function up()
    {
        Schema::create('vue_vue_author', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('about');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vue_vue_author');
    }
}
