<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVuePostTags2 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_post_tags', function($table)
        {
            $table->dropPrimary(['posts_id','tag_id']);
            $table->renameColumn('tag_id', 'tags_id');
            $table->primary(['posts_id','tags_id']);
        });
    }
    
    public function down()
    {
        Schema::table('vue_vue_post_tags', function($table)
        {
            $table->dropPrimary(['posts_id','tags_id']);
            $table->renameColumn('tags_id', 'tag_id');
            $table->primary(['posts_id','tag_id']);
        });
    }
}
