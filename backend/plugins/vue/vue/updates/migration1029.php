<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1029 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_post_tags', function($table)
        {
            $table->foreign('posts_id', 'post_tags_foreign')->references('id')->on('vue_vue_posts')->onDelete('cascade');
            $table->foreign('tags_id', 'tags_post_foreign')->references('id')->on('vue_vue_categories')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('vue_vue_post_tags', function($table)
        {
            $table->dropForeign('post_tags_foreign');
            $table->dropForeign('post_tags_foreign');
        });
    }
}