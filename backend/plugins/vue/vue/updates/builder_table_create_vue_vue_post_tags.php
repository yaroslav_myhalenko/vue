<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateVueVuePostTags extends Migration
{
    public function up()
    {
        Schema::create('vue_vue_post_tags', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('post_id');
            $table->integer('tag_id');
            $table->primary(['post_id','tag_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('vue_vue_post_tags');
    }
}
