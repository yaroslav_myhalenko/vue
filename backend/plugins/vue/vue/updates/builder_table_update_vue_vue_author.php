<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVueAuthor extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_author', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('vue_vue_author', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
