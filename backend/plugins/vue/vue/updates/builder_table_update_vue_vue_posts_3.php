<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVuePosts3 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_posts', function($table)
        {
            $table->boolean('is_top');
            $table->boolean('is_show');
        });
    }
    
    public function down()
    {
        Schema::table('vue_vue_posts', function($table)
        {
            $table->dropColumn('is_top');
            $table->dropColumn('is_show');
        });
    }
}
