<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVuePostCategories extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_post_categories', function($table)
        {
            $table->integer('posts_id')->unsigned()->change();
            $table->integer('category_id')->unsigned()->change();
        });
    }

    public function down()
    {
        Schema::table('vue_vue_post_categories', function($table)
        {
            $table->integer('posts_id')->unsigned(false)->change();
            $table->integer('category_id')->unsigned(false)->change();
        });
    }
}