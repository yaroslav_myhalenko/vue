<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVuePosts extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_posts', function($table)
        {
            $table->integer('author_id');
            $table->date('date')->nullable(false)->unsigned(false)->default(null)->change();
            $table->dropColumn('author');
            $table->dropColumn('img');
        });
    }
    
    public function down()
    {
        Schema::table('vue_vue_posts', function($table)
        {
            $table->dropColumn('author_id');
            $table->dateTime('date')->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('author', 191);
            $table->string('img', 191);
        });
    }
}
