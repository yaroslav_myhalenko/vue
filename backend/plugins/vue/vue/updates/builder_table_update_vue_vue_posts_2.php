<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVuePosts2 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_posts', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('vue_vue_posts', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
