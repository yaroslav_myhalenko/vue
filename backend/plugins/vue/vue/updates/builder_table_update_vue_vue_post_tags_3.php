<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVuePostTags3 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_post_tags', function($table)
        {
            $table->integer('posts_id')->unsigned()->change();
            $table->integer('tags_id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('vue_vue_post_tags', function($table)
        {
            $table->integer('posts_id')->unsigned(false)->change();
            $table->integer('tags_id')->unsigned(false)->change();
        });
    }
}
