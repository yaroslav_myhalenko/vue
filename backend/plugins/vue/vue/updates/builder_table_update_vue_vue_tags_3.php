<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateVueVueTags3 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_tags', function($table)
        {
            $table->boolean('is_enabled');
        });
    }
    
    public function down()
    {
        Schema::table('vue_vue_tags', function($table)
        {
            $table->dropColumn('is_enabled');
        });
    }
}
