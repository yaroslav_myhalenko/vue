<?php namespace Vue\Vue\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1023 extends Migration
{
    public function up()
    {
        Schema::table('vue_vue_categories', function($table)
        {
            $table->index('slug');
            $table->index('is_show');
            $table->index('is_menu');
            $table->index('is_enabled');
        });
    }

    public function down()
    {
        Schema::table('vue_vue_categories', function($table)
        {
            $table->dropIndex('slug');
            $table->dropIndex('is_show');
            $table->dropIndex('is_menu');
            $table->dropIndex('is_enabled');
        });
    }
}